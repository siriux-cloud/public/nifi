# syntax=docker/dockerfile:1

ARG NIFI_VERSION=latest

# Agregamos Step Cli para iteractuar con Step CA
FROM smallstep/step-cli as step

# Basado en la ultima version de Nifi!
FROM apache/nifi:${NIFI_VERSION}

# Descargamos el conector de mysql
ARG MYSQL_CONNECTOR_JAVA_VERSION="8.2.0"
RUN curl -f -L -o "${NIFI_HOME}/lib/mysql-connector-j-${MYSQL_CONNECTOR_JAVA_VERSION}.jar" \
    "https://repo1.maven.org/maven2/com/mysql/mysql-connector-j/${MYSQL_CONNECTOR_JAVA_VERSION}/mysql-connector-j-${MYSQL_CONNECTOR_JAVA_VERSION}.jar"

# Driver para sqlite
ARG SQLITE_JDBC_DRIVER_VERSION="3.43.2.1"
RUN curl -f -L -o "${NIFI_HOME}/lib/sqlite-jdbc-${SQLITE_JDBC_DRIVER_VERSION}.jar" \
    "https://github.com/xerial/sqlite-jdbc/releases/download/${SQLITE_JDBC_DRIVER_VERSION}/sqlite-jdbc-${SQLITE_JDBC_DRIVER_VERSION}.jar"

# Driver para postgres
ARG POSTGRESQL_JDBC_DRIVER_VERSION="42.6.0"
RUN curl -f -L -o "${NIFI_HOME}/lib/postgresql-${POSTGRESQL_JDBC_DRIVER_VERSION}.jar" \
    "https://jdbc.postgresql.org/download/postgresql-${POSTGRESQL_JDBC_DRIVER_VERSION}.jar"

# Driver para mssqlserver
ARG SQLSERVER_CONNECTOR_JAVA_VERSION="12.4.2"
RUN curl -f -L -o "${NIFI_HOME}/lib/mssql-jdbc-${SQLSERVER_CONNECTOR_JAVA_VERSION}.jre11.jar" \
    "https://github.com/microsoft/mssql-jdbc/releases/download/v${SQLSERVER_CONNECTOR_JAVA_VERSION}/mssql-jdbc-${SQLSERVER_CONNECTOR_JAVA_VERSION}.jre11.jar"

# Usamos Root
USER root

# Instalar step
COPY --from=step /usr/local/bin/step /usr/local/bin/

# Instalamos gomplate para manipular templates
ARG GOMPLATE_VERSION="3.11.5"
RUN wget https://github.com/hairyhenderson/gomplate/releases/download/v${GOMPLATE_VERSION}/gomplate_linux-amd64 -O /usr/bin/gomplate && \
    chmod +x /usr/bin/gomplate

ARG YQ_VERSION="4.35.2"
RUN wget https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64 -O /usr/bin/yq && \
    chmod +x /usr/bin/yq

# Instalar dependencias del SO
RUN set -eux; \
        apt-get update -y; \
        apt-get install -y --no-install-recommends \
            ca-certificates \
            gnupg \
            netbase \
            wget \
            tzdata \
            curl \
            jq \
            git \
            gosu \
            rsync \
            sqlite3 \
            libsqlite3-dev \
            nano \
            python3 \
            python3-pip \
            python3-venv \
            pipx \
            python-is-python3 \
            uuid-runtime \
        ; \
        rm -rf /var/lib/apt/lists/*;

# Instalamos La version de Python del sistema
ARG PIPX_TOOLS=""
ENV PIPX_HOME=/usr/local/py-utils
ENV PIPX_BIN_DIR=/usr/local/py-utils/bin
ENV PATH=${PATH}:${PIPX_BIN_DIR}
RUN <<EOT
#!/bin/bash -e
    # Creamos la carpeta de python tools
    mkdir -p "${PIPX_BIN_DIR}"

    # Cambiamos el separador de campo interno (IFS) a espacio en blanco
    IFS=' '

    # Se asignan los elementos a los argumentos posicionales
    set -- ${PIPX_TOOLS}

    # Recorrer la lista de utilidades con un bucle while
    while [ $# -gt 0 ]; do
        util="$1"
        if ! command -v "$util" > /dev/null 2>&1; then
            echo "${util} installing..."
            pipx install --system-site-packages --pip-args '--no-cache-dir --force-reinstall' ${util}
        else
            echo "${util} already installed. Skipping."
        fi
        shift # Mover al siguiente argumento
    done
EOT

# Agregamos Herramientas adicionales
ONBUILD ARG PIPX_TOOLS=""
ONBUILD USER root
ONBUILD RUN <<EOT
#!/bin/bash -e
    # Cambiamos el separador de campo interno (IFS) a espacio en blanco
    IFS=' '
    # Se asignan los elementos a los argumentos posicionales
    set -- ${PIPX_TOOLS}
    # Recorrer la lista de utilidades con un bucle while
    while [ $# -gt 0 ]; do
        util="$1"
        if ! command -v "$util" > /dev/null 2>&1; then
            echo "${util} installing..."
            pipx install --system-site-packages --pip-args '--no-cache-dir --force-reinstall' ${util}
        else
            echo "${util} already installed. Skipping."
        fi
        shift # Mover al siguiente argumento
    done
EOT

# Instalamos Node 20 y NPM
ARG NODE_MAJOR=20
ARG NPM_MAJOR=10
RUN <<EOT
#!/bin/bash -e
    mkdir -p /etc/apt/keyrings
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_${NODE_MAJOR}.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
    apt-get update -y
    apt-get install -y nodejs
    rm -rf /var/lib/apt/lists/*
    npm install -g npm@${NPM_MAJOR} 
EOT

# Agregamos Herramientas adicionales
ONBUILD ARG NPM_INSTALL=""
ONBUILD USER root
ONBUILD RUN <<EOT
#!/bin/bash -e
    # Cambiamos el separador de campo interno (IFS) a espacio en blanco
    IFS=' '
    # Se asignan los elementos a los argumentos posicionales
    set -- ${NPM_INSTALL}
    # Recorrer la lista de utilidades con un bucle while
    while [ $# -gt 0 ]; do
        util="$1"
        echo "Instalando: ${util}"
        npm install -g ${util}
        shift # Mover al siguiente argumento
    done
EOT

RUN <<EOT
#!/bin/bash -eux
    MARK_COMMENT="# Continuously provide logs so that 'docker logs' can produce them"
    if grep -q -F "${MARK_COMMENT}" "${NIFI_BASE_DIR}/scripts/start.sh"; then
        sed -n "/${MARK_COMMENT}/q;p" "${NIFI_BASE_DIR}/scripts/start.sh" > "${NIFI_BASE_DIR}/scripts/start-dev.sh"
        echo '"${NIFI_HOME}/bin/nifi.sh" run' >> "${NIFI_BASE_DIR}/scripts/start-dev.sh"
        chmod +x "${NIFI_BASE_DIR}/scripts/start-dev.sh"
        chown -R nifi:nifi "${NIFI_BASE_DIR}/scripts/start-dev.sh"
    else
        echo "Comentario not found in ${NIFI_BASE_DIR}/scripts/start.sh, El archivo ha sido modificado en esta versión de Nifi:  ${NIFI_VERSION}."
        exit 1
    fi
EOT

# Copiamos archivos del usuasrio root
COPY root/ /

# Copiamos archivos del usuasrio nifi
COPY --chown=nifi:nifi nifi/ /

# ADDING TRUST internal certificate
ONBUILD USER root
ONBUILD ARG CA_URL
ONBUILD ARG CA_FINGERPRINT
ONBUILD ENV CA_URL=${CA_URL}
ONBUILD ENV CA_FINGERPRINT=${CA_FINGERPRINT}
ONBUILD RUN <<EOT
#!/bin/bash -e
    if [ -n "${CA_URL}" ]; then
        echo "CA_URL: ${CA_URL}"
        echo "CA_FINGERPRINT: ${CA_FINGERPRINT}"
        mkdir -p /opt/step-ca
        STEPPATH=/opt/step-ca step ca bootstrap --ca-url "${CA_URL}" --fingerprint "${CA_FINGERPRINT}" --install --force
        chown -R nifi:nifi /opt/step-ca
    fi
EOT
ONBUILD USER nifi

USER nifi

# Mostramos el id de nifi
RUN id

# Creamos un conf repository para aquellos datos que pueden ser modificados en tiempo de ejecucion
RUN <<EOT
#!/bin/bash -eux
    mkdir -p "${NIFI_HOME}/conf_repository"
EOT

VOLUME ["${NIFI_HOME}/conf_repository"]

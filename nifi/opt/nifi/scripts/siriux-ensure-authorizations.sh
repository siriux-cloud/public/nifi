#!/bin/bash

# Direccion del script para sh POSIX (no bash),
scripts_dir=$(dirname $0)

if [ -n "${SIRIUX_DEVELOPMENT_MODE:-}" ]; then
    echo "Modo de desarrollo activado, no se puede continuar con el proceso" >&2
    exit 0
fi

[ -f "${scripts_dir}/common.sh" ] && . "${scripts_dir}/common.sh"
[ -f "${scripts_dir}/siriux-common.sh" ] && . "${scripts_dir}/siriux-common.sh"

# Debemos esperar que el el archivo users.xml exista en nifi/conf_repository y tambien que el archivo authorizations.xml exista en nifi/conf_repository
# ambos archivo deben tener un periodo superior a 30 segundos para poder continuar con el proceso.

# NIFI_REGISTRY_AUTHORIZERS_FILE_ACCESS_POLICY_PROVIDER_AUTHORIZATIONS_FILE="./auth.xml"
# NIFI_REGISTRY_AUTHORIZERS_FILE_USER_GROUP_PROVIDER_USER_FILE="./users.xml"
# INITIAL_ADMIN_GROUP_NAME="Operators"
# INITIAL_ADMIN_IDENTITY="owner.siriux@gmail.com"
# INITIAL_USERS_IDENTITIES="fulanito@imolko.net menganito@gmail.com leonardo@jdl.com"
# NIFI_HOME=`pwd`

auth_file="$(absolute_nifi_path "${NIFI_REGISTRY_AUTHORIZERS_FILE_ACCESS_POLICY_PROVIDER_AUTHORIZATIONS_FILE:-./conf_repository/authorizations.xml}")"
user_file="$(absolute_nifi_path "${NIFI_REGISTRY_AUTHORIZERS_FILE_USER_GROUP_PROVIDER_USER_FILE:-./conf_repository/users.xml}")"

while true; do
    if  [ ! -f "$user_file" ]; then
        echo "Esperemos hasta que el archivo $user_file sea creado." >&2
        sleep 10
        continue
    fi

    if  [ ! -f "$auth_file" ]; then
        echo "Esperemos hasta que el archivo $auth_file sea creado." >&2
        sleep 10
        continue
    fi

    # Si el perido de los archivos es superior a 1 minuto, debemos esperar
    if [ $(find "$user_file" -mmin +1 | wc -l) -eq 0 ]; then
        echo "Esperemos hasta que el archivo $user_file no halla sido modificado en un periodo de 1 minuto." >&2
        sleep 10
        continue
    fi

    if [ $(find "$auth_file" -mmin +1 | wc -l) -eq 0 ]; then
        echo "Esperemos hasta que el archivo $auth_file no halla sido modificado en un periodo de 1 minuto." >&2
        sleep 10
        continue
    fi

    # Si no existen los nodos users y groups, se deben agregar.
    if [ -z "$(xmlstarlet sel -t -m "/tenants/users" -c "." "$user_file")" ]; then
        echo "No existe el nodo users en el archivo $user_file, se debe agregar." >&2
        if [ -z "$(xmlstarlet sel -t -m "/tenants/groups" -c "." "$user_file")" ]; then
            xmlstarlet ed -L -s "/tenants" -t elem -n "users"  -v "" "$user_file"
        else
            xmlstarlet ed -L -a "/tenants/groups" -t elem -n "users"  -v "" "$user_file"
        fi    
    fi
    if [ -z "$(xmlstarlet sel -t -m "/tenants/groups" -c "." "$user_file")" ]; then
        xmlstarlet ed -L -i "/tenants/users[1]" -t elem -n "groups" -v "" "$user_file"
    fi

    # Obtenemos entonces el grupo a crear solo si es especificado.
    initial_group="${INITIAL_ADMIN_GROUP_NAME}"
    if [ -n "$initial_group" ]; then
        group_identifier="$(xmlstarlet sel -t -m "/tenants/groups/group[@name='${initial_group}']" -v "@identifier" "$user_file")"
        if [ -z "${group_identifier}" ]; then
            # Creamos el grupo Admin Inicial
            group_identifier=$(uuidgen)

            # Agregamos el Grupo
            xmlstarlet ed -L \
                -s "/tenants/groups" -t elem -n "group_temp" -v "" \
                -i "/tenants/groups/group_temp[last()]" -t attr -n "identifier" -v "$group_identifier" \
                -i "/tenants/groups/group_temp[last()]" -t attr -n "name" -v "$initial_group" \
                -r "/tenants/groups/group_temp" -v "group" "$user_file"
        fi

        if [ -z "${group_identifier}" ]; then
            echo "No se pudo crear el grupo ${initial_group}, reintentamos" >&2
            sleep 10
            continue
        fi
    fi

    if [ -n "${group_identifier}" ]; then
        # Creamos las policy para el grupo
        initial_admin="${INITIAL_ADMIN_IDENTITY}"
        if [ -n "$initial_admin" ]; then
            admin_identifier="$(xmlstarlet sel -t -m "/tenants/users/user[@identity='${initial_admin}']" -v "@identifier" "$user_file")"
            if [ -z "${admin_identifier}" ]; then
                echo "El usuario ${initial_admin} no existe en el archivo ${user_file}." >&2
                sleep 10
                continue
            fi

            # Verificamos si tiene alguna policy asociada, si no hay policy para el usuario admin debemos abortar
            if [ -z "$(xmlstarlet sel -t -m "//policy[user/@identifier='${admin_identifier}']" -c "." "$auth_file")" ]; then
                echo "El usuario ${initial_admin} no tiene ninguna policy asociada en el archivo ${auth_file}." >&2
                sleep 10
                continue
            fi

            # Creamos las policy para el grupo equivalentes a las del usuario initial_admin
           xmlstarlet ed -L \
               -i "//policy[user/@identifier='${admin_identifier}' and not(group/@identifier='${group_identifier}')]/user[1]" -t elem -n "group" -v "" \
               -i "//policy[user/@identifier='${admin_identifier}' and not(group/@identifier='${group_identifier}')]/group[last()]" -t attr -n "identifier" -v "${group_identifier}" \
               "$auth_file"
        fi
    fi

    # initial users estan separados por espacios
    initial_users="${INITIAL_USERS_IDENTITIES}"
    if [ -n "$initial_users" ]; then
        for initial_user in $initial_users; do
            user_identifier="$(xmlstarlet sel -t -m "/tenants/users/user[@identity='${initial_user}']" -v "@identifier" "$user_file")"
            if [ -z "${user_identifier}" ]; then
                # Creamos el usuario
                user_identifier=$(uuidgen)

                # Agregamos el Usuario
                xmlstarlet ed -L \
                    -s "/tenants/users" -t elem -n "user_temp" -v "" \
                    -i "/tenants/users/user_temp[last()]" -t attr -n "identifier" -v "$user_identifier" \
                    -i "/tenants/users/user_temp[last()]" -t attr -n "identity" -v "$initial_user" \
                    -r "/tenants/users/user_temp" -v "user" "$user_file"
            fi

            # Se especifico un grupo?
            if [ -n "${group_identifier}" ]; then
                # Agrergamos el usuario al grupo si no existe
                xmlstarlet ed -L \
                    -s "/tenants/groups/group[@identifier='${group_identifier}' and not(user/@identifier='${user_identifier}')]" -t elem -n "user_tmp" -v "" \
                    -i "/tenants/groups/group[@identifier='${group_identifier}' and not(user/@identifier='${user_identifier}')]/user_tmp" -t attr -n "identifier" -v "${user_identifier}" \
                    -r "/tenants/groups/group[@identifier='${group_identifier}' and not(user/@identifier='${user_identifier}')]/user_tmp" -v "user" \
                    "$user_file"
            fi
        done
    fi

    break
done

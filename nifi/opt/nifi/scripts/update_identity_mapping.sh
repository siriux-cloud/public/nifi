#!/bin/bash -e

# El ultimo grupo de mapping tratado
last_mapping_group=""

# Creamos un mecanismo para manejar [Identity Mapping Properties](https://nifi.apache.org/docs/nifi-docs/html/administration-guide.html#identity-mapping-properties).
# Podemos definir variables de la forma NIFI_SECURITY_IDENTITY_MAPPING_<proveedor de identidad>_<propiedad>,
# Estas se definen de forma inversa que en nifi properties, es decir, primero el proveedor de identidad y luego la propiedad.
# Por ejemplo, para definir la propiedad nifi.security.identity.mapping.pattern.oidc=^.*CN=(.*?).*
# debemos definir la variable NIFI_SECURITY_IDENTITY_MAPPING_OIDC_PATTERN="^.*CN=(.*?).*" 
# Como vemos se convierten en minusculas, y esta forma es facil agruparlas por proveedor de identidad.
for variable in $(env | grep -E '^NIFI_SECURITY_IDENTITY_MAPPING_[^_]+_.*$' | sort); do
    # Extraer el nombre y el valor de la variable
    name=$(echo "$variable" | cut -d'=' -f1)
    valor=$(echo "$variable" | cut -d'=' -f2-)
    grupo=$(echo "$name" | cut -d'_' -f5)
    
    # Formatear la línea recordar en el name debe ir el nombre en minúsculas y remplazar _ por . (punto)
    linea="nifi.security.identity.mapping.$(echo "$name" | cut -d'_' -f6 | tr '[:upper:]' '[:lower:]').$(echo "$name" | cut -d'_' -f5 | tr '[:upper:]' '[:lower:]')=$(echo "$valor")"

    echo "Agregando o actualizando la propiedad $linea en el archivo ${nifi_props_file}"
    if [ "$last_mapping_group" != "$grupo" ]; then
        if [ "$last_mapping_group" = "" ]; then
            echo "" >> "${nifi_props_file}"
        fi
        echo "" >> "${nifi_props_file}"
        echo "# Identity Mapping $grupo" >> "${nifi_props_file}"
        last_mapping_group="$grupo"
    fi

    # Agregar o actualizar en el archivo de configuración
    echo "$linea" >> "$nifi_props_file"
done

#!/bin/bash -e

# Direccion del script para sh POSIX (no bash),
scripts_dir='/opt/nifi/scripts'

[ -f "${scripts_dir}/common.sh" ] && . "${scripts_dir}/common.sh"
[ -f "${scripts_dir}/siriux-common.sh" ] && . "${scripts_dir}/siriux-common.sh"

# Copia de seguridad para los archivos de configuraciones
conf_original_backup

# Restablecemos los archivos de configuración originales
cp "${NIFI_HOME}/conf-original/nifi.properties" "${NIFI_HOME}/conf/"
cp "${NIFI_HOME}/conf-original/bootstrap.conf" "${NIFI_HOME}/conf/"
cp "${NIFI_HOME}/conf-original/authorizers.xml" "${NIFI_HOME}/conf/"
cp "${NIFI_HOME}/conf-original/login-identity-providers.xml" "${NIFI_HOME}/conf/"

# Reseteamos los usuarios y las autorizaciones
if [ -n "${SIRIUX_DEVELOPMENT_MODE:-}" ]; then
    # Debemos elimnar el archivo de user.xml, para que siempre se cree el usuario admin asociado al certificado usado
    user_file="$(absolute_nifi_path "${NIFI_REGISTRY_AUTHORIZERS_FILE_USER_GROUP_PROVIDER_USER_FILE:-./conf_repository/users.xml}")"
    if [ -f "${user_file}" ]; then
        rm -f "${user_file}"
    fi

    # Debemos elimnar el archivo de authorizations.xml, para que siempre se autorize el usuario admin asociado al certificado usado
    auth_file="$(absolute_nifi_path "${NIFI_REGISTRY_AUTHORIZERS_FILE_ACCESS_POLICY_PROVIDER_AUTHORIZATIONS_FILE:-./conf_repository/authorizations.xml}")"
    if [ -f "${auth_file}" ]; then
        rm -f "${auth_file}"
    fi
fi

# Core Properties #
prop_replace 'nifi.flow.configuration.file'                     "${NIFI_FLOW_CONFIGURATION_FILE:-./conf_repository/flow.xml.gz}"
ensure_conf_file "${NIFI_FLOW_CONFIGURATION_FILE:-./conf_repository/flow.xml.gz}" "${NIFI_HOME}/conf-original/flow.xml.gz"

prop_replace 'nifi.flow.configuration.json.file'                "${NIFI_FLOW_CONFIGURATION_JSON_FILE:-./conf_repository/flow.json.gz}"
ensure_conf_file "${NIFI_FLOW_CONFIGURATION_JSON_FILE:-./conf_repository/flow.json.gz}" "${NIFI_HOME}/conf-original/flow.json.gz"

# Aseguramos la creacion iniciak del archivo flow.xml.gz
ensure_flow_xml "${NIFI_FLOW_GROUPROOT_NAME:-Siriux Flow}" "${NIFI_FLOW_CONFIGURATION_FILE:-./conf_repository/flow.xml.gz}" "${NIFI_FLOW_CONFIGURATION_JSON_FILE:-./conf_repository/flow.json.gz}"

prop_replace 'nifi.flow.configuration.archive.enabled'          "${NIFI_FLOW_CONFIGURATION_ARCHIVE_ENABLED:-true}"
prop_replace 'nifi.flow.configuration.archive.dir'              "${NIFI_FLOW_CONFIGURATION_ARCHIVE_DIR:-./conf_repository/archive/}"
prop_replace 'nifi.flow.configuration.archive.max.time'         "${NIFI_FLOW_CONFIGURATION_ARCHIVE_MAX_TIME:-30 days}"
prop_replace 'nifi.flow.configuration.archive.max.storage'      "${NIFI_FLOW_CONFIGURATION_ARCHIVE_MAX_STORAGE:-500 MB}"
prop_replace 'nifi.flow.configuration.archive.max.count'        "${NIFI_FLOW_CONFIGURATION_ARCHIVE_MAX_COUNT:-}"
prop_replace 'nifi.flowcontroller.autoResumeState'              "${NIFI_FLOWCONTROLLER_AUTORESUMESTATE:-true}"
prop_replace 'nifi.flowcontroller.graceful.shutdown.period'     "${NIFI_FLOWCONTROLLER_GRACEFUL_SHUTDOWN_PERIOD:-10 sec}"
prop_replace 'nifi.flowservice.writedelay.interval'             "${NIFI_FLOWSERVICE_WRITEDELAY_INTERVAL:-500 ms}"
prop_replace 'nifi.administrative.yield.duration'               "${NIFI_ADMINISTRATIVE_YIELD_DURATION:-30 sec}"
# If a component has no work to do (is "bored"), how long should we wait before checking again for work?
prop_replace 'nifi.bored.yield.duration'                        "${NIFI_BORED_YIELD_DURATION:-10 millis}"
prop_replace 'nifi.queue.backpressure.count'                    "${NIFI_QUEUE_BACKPRESSURE_COUNT:-10000}"
prop_replace 'nifi.queue.backpressure.size'                     "${NIFI_QUEUE_BACKPRESSURE_SIZE:-1 GB}"

prop_replace 'nifi.authorizer.configuration.file'               "${NIFI_AUTHORIZER_CONFIGURATION_FILE:-./conf/authorizers.xml}"
prop_replace 'nifi.login.identity.provider.configuration.file'  "${NIFI_LOGIN_IDENTITY_PROVIDER_CONFIGURATION_FILE:-./conf/login-identity-providers.xml}"
prop_replace 'nifi.templates.directory'                         "${NIFI_TEMPLATES_DIRECTORY:-./conf_repository/templates}"
prop_replace 'nifi.ui.banner.text'                              "${NIFI_UI_BANNER_TEXT:-}"
prop_replace 'nifi.ui.autorefresh.interval'                     "${NIFI_UI_AUTOREFRESH_INTERVAL:-30 sec}"
prop_replace 'nifi.nar.library.directory'                       "${NIFI_NAR_LIBRARY_DIRECTORY:-./lib}"
prop_replace 'nifi.nar.library.autoload.directory'              "${NIFI_NAR_LIBRARY_AUTOLOAD_DIRECTORY:-./extensions}"
prop_replace 'nifi.nar.working.directory'                       "${NIFI_NAR_WORKING_DIRECTORY:-./work/nar/}"
prop_replace 'nifi.documentation.working.directory'             "${NIFI_DOCUMENTATION_WORKING_DIRECTORY:-./work/docs/components}"
prop_replace 'nifi.nar.unpack.uber.jar'                         "${NIFI_NAR_UNPACK_UBER_JAR:-false}"

# security properties #
prop_replace 'nifi.sensitive.props.key'                         "${NIFI_SENSITIVE_PROPS_KEY:-1234567890}"
prop_replace 'nifi.sensitive.props.key.protected'               "${NIFI_SENSITIVE_PROPS_KEY_PROTECTED:-}"
prop_replace 'nifi.sensitive.props.algorithm'                   "${NIFI_SENSITIVE_PROPS_ALGORITHM:-NIFI_PBKDF2_AES_GCM_256}"
prop_replace 'nifi.sensitive.props.additional.keys'             "${NIFI_SENSITIVE_PROPS_ADDITIONAL_KEYS:-}"

# content repository properties
prop_replace 'nifi.content.repository.archive.enabled'          "${NIFI_CONTENT_REPOSITORY_ARCHIVE_ENABLED:-true}"

# provenance repository properties
prop_replace 'nifi.provenance.repository.directory.default'        "${NIFI_PROVENANCE_REPOSITORY_DIRECTORY_DEFAULT:-./provenance_repository}"
prop_replace 'nifi.provenance.repository.max.storage.time'         "${NIFI_PROVENANCE_REPOSITORY_MAX_STORAGE_TIME:-30 days}"
prop_replace 'nifi.provenance.repository.max.storage.size'         "${NIFI_PROVENANCE_REPOSITORY_MAX_STORAGE_SIZE:-1 GB}"
prop_replace 'nifi.provenance.repository.rollover.time'            "${NIFI_PROVENANCE_REPOSITORY_ROLLOVER_TIME:-10 mins}"
prop_replace 'nifi.provenance.repository.rollover.size'            "${NIFI_PROVENANCE_REPOSITORY_ROLLOVER_SIZE:-100 MB}"
prop_replace 'nifi.provenance.repository.query.threads'            "${NIFI_PROVENANCE_REPOSITORY_QUERY_THREADS:-2}"
prop_replace 'nifi.provenance.repository.index.threads'            "${NIFI_PROVENANCE_REPOSITORY_INDEX_THREADS:-2}"
prop_replace 'nifi.provenance.repository.compress.on.rollover'     "${NIFI_PROVENANCE_REPOSITORY_COMPRESS_ON_ROLLOVER:-true}"
prop_replace 'nifi.provenance.repository.always.sync'              "${NIFI_PROVENANCE_REPOSITORY_ALWAYS_SYNC:-false}"
prop_replace 'nifi.provenance.repository.indexed.fields'           "${NIFI_PROVENANCE_REPOSITORY_INDEXED_FIELDS:-EventType, FlowFileUUID, Filename, ProcessorID, Relationship}"
prop_replace 'nifi.provenance.repository.indexed.attributes'       "${NIFI_PROVENANCE_REPOSITORY_INDEXED_ATTRIBUTES:-}"
prop_replace 'nifi.provenance.repository.index.shard.size'         "${NIFI_PROVENANCE_REPOSITORY_INDEX_SHARD_SIZE:-500 MB}"
prop_replace 'nifi.provenance.repository.max.attribute.length'     "${NIFI_PROVENANCE_REPOSITORY_MAX_ATTRIBUTE_LENGTH:-65536}"
prop_replace 'nifi.provenance.repository.concurrent.merge.threads' "${NIFI_PROVENANCE_REPOSITORY_CONCURRENT_MERGE_THREADS:-2}"
prop_replace 'nifi.provenance.repository.buffer.size'              "${NIFI_PROVENANCE_REPOSITORY_BUFFER_SIZE:-100000}"

# Ejecutamos los mapping de indentity
echo "Establecer los mapping de indentity"
[ -f "${scripts_dir}/update_identity_mapping.sh" ] && . "${scripts_dir}/update_identity_mapping.sh"

# Mofdificamos authorizers.xml
echo "Modificamos authorizers.xml"
[ -f "${scripts_dir}/update_authorizer.sh" ] && . "${scripts_dir}/update_authorizer.sh"

if [ -n "${SIRIUX_DEVELOPMENT_MODE:-}" ]; then
    # Enviamos el control a star.sh
    exec "${scripts_dir}/start-dev.sh" ${@}
else
    # Enviamos el control a star.sh
    exec "${scripts_dir}/start.sh" ${@}
fi

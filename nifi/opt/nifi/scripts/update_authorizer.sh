#!/bin/sh -e

providers_file=${NIFI_HOME}/conf/authorizers.xml
property_xpath='/authorizers'

fugp_path="${property_xpath}/userGroupProvider[identifier='file-user-group-provider']"
if xmlstarlet sel -t -m "${fugp_path}" -c '.' "${providers_file}" > /dev/null; then
    xmlstarlet ed --pf --inplace -u "${fugp_path}/property[@name='Users File']" -v "${NIFI_REGISTRY_AUTHORIZERS_FILE_USER_GROUP_PROVIDER_USER_FILE:-./conf_repository/users.xml}" "${providers_file}"
    xmlstarlet ed --pf --inplace -u "${fugp_path}/property[@name='Initial User Identity 1']" -v "${INITIAL_ADMIN_IDENTITY}" "${providers_file}"
fi

fapp_path="${property_xpath}/accessPolicyProvider[identifier='file-access-policy-provider']"
if xmlstarlet sel -t -m "${fapp_path}" -c '.' "${providers_file}" > /dev/null; then
    xmlstarlet ed --pf --inplace -u "${fapp_path}/property[@name='Authorizations File']" -v "${NIFI_REGISTRY_AUTHORIZERS_FILE_ACCESS_POLICY_PROVIDER_AUTHORIZATIONS_FILE:-./conf_repository/authorizations.xml}" "${providers_file}"
    xmlstarlet ed --pf --inplace -u "${fapp_path}/property[@name='Initial Admin Identity']" -v "${INITIAL_ADMIN_IDENTITY}" "${providers_file}"
    if [ -n "${NODE_IDENTITY}" ]; then
        xmlstarlet ed --pf --inplace -u "${fapp_path}/property[@name='Node Identity 1']" -v "${NODE_IDENTITY}" "${providers_file}"
    fi
fi

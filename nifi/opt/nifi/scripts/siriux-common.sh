#!/bin/sh -e

conf_original_backup() {
    # Copia de seguridad para los archivos de configuraciones
    if [ ! -d "${NIFI_HOME}/conf-original" ]; then
        echo "Creando copia de seguridad de la carpeta conf" >&2
        temp_dir=$(mktemp -d)
        cp -r "${NIFI_HOME}/conf" "${temp_dir}/conf-original"
        mv "${temp_dir}/conf-original" "${NIFI_HOME}/conf-original"
        rm -rf "${temp_dir}"
    fi
}

absolute_nifi_path() {
    echo "$(cd ${NIFI_HOME} && realpath -m "$1")"
}

ensure_conf_file() {
    # Reemplaza una propiedad en un archivo de configuración si no existe
    # $1: archivo de configuración
    # $2: copia de seguridad
    f1="$(absolute_nifi_path "$1")"
    f2="$(absolute_nifi_path "$2")"
    if [ ! -f "$f1" ]; then
        if [ -f "$f2" ]; then
            cp "$f2" "$f1"
        else
            echo "El archivo $f1 no existe y no se puede restaurar desde $f2" >&2
        fi
    fi
}

uuid() {
    if command -v uuidgen > /dev/null; then
        uuidgen
    else
        if [ -f /proc/sys/kernel/random/uuid ]; then
            cat /proc/sys/kernel/random/uuid
        else
            echo "No se puede generar un UUID" >&2
            return 1
        fi
    fi
}

ensure_flow_xml() {
    nameflow="${1:-NiFi Flow}"
    fxml="${2}"
    fjson="${3}"

    if [ -z "${nameflow}" ]; then
        nameflow="NiFi Flow"
    fi

    # Estos no deben existir ninguno de los dos, si existe uno y el otro no, no debemos continuar.
    if [ -f "${fxml}" ] && [ ! -f "${fjson}" ]; then
        echo "El archivo ${fxml} existe, pero el archivo ${fjson} no existe, no se puede continuar" >&2
        return 1
    fi

    if [ -f "${fjson}" ] && [ ! -f "${fxml}" ]; then
        echo "El archivo ${fjson} existe, pero el archivo ${fxml} no existe, no se puede continuar" >&2
        return 1
    fi

    # Si ambos archivos existen, continuamos sin hacer nada
    if [ -f "${fxml}" ] && [ -f "${fjson}" ]; then
        return 0
    fi

    # generamos un uid para el archivo fxml
    identifier=$(uuid)
    instanceIdentifier=$(uuid)

    # Si no existe ninguno de los dos, creammos el archivo fxml y el archivo fjson a partir de un HERODOC sustituyendo las variables.
    (gzip -c -)> "${fxml}" <<EOF
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<flowController encoding-version="1.4">
  <maxTimerDrivenThreadCount>10</maxTimerDrivenThreadCount>
  <maxEventDrivenThreadCount>1</maxEventDrivenThreadCount>
  <registries/>
  <parameterContexts/>
  <rootGroup>
    <id>${instanceIdentifier}</id>
    <name>${nameflow:-Nifi Flow}</name>
    <position x="0.0" y="0.0"/>
    <comment/>
    <flowfileConcurrency>UNBOUNDED</flowfileConcurrency>
    <flowfileOutboundPolicy>STREAM_WHEN_AVAILABLE</flowfileOutboundPolicy>
    <defaultFlowFileExpiration>0 sec</defaultFlowFileExpiration>
    <defaultBackPressureObjectThreshold>10000</defaultBackPressureObjectThreshold>
    <defaultBackPressureDataSizeThreshold>1 GB</defaultBackPressureDataSizeThreshold>
    <logFileSuffix/>
  </rootGroup>
  <controllerServices/>
  <reportingTasks/>
  <parameterProviders/>
</flowController>
EOF

    (jq -r | gzip -c -)> "${fjson}" <<EOF
{
  "encodingVersion": {
    "majorVersion": 2,
    "minorVersion": 0
  },
  "maxTimerDrivenThreadCount": 10,
  "maxEventDrivenThreadCount": 1,
  "registries": [],
  "parameterContexts": [],
  "parameterProviders": [],
  "controllerServices": [],
  "reportingTasks": [],
  "templates": [],
  "rootGroup": {
    "identifier": "${identifier}",
    "instanceIdentifier": "${instanceIdentifier}",
    "name": "${nameflow:-Nifi Flow}",
    "comments": "",
    "position": {
      "x": 0,
      "y": 0
    },
    "processGroups": [],
    "remoteProcessGroups": [],
    "processors": [],
    "inputPorts": [],
    "outputPorts": [],
    "connections": [],
    "labels": [],
    "funnels": [],
    "controllerServices": [],
    "variables": {},
    "defaultFlowFileExpiration": "0 sec",
    "defaultBackPressureObjectThreshold": 10000,
    "defaultBackPressureDataSizeThreshold": "1 GB",
    "componentType": "PROCESS_GROUP",
    "flowFileConcurrency": "UNBOUNDED",
    "flowFileOutboundPolicy": "STREAM_WHEN_AVAILABLE"
  }
}
EOF
}


# Función para esperar a que un archivo exista
wait_for_file() {
    local file=$1
    local message="Esperando a que el archivo $file exista"

    while [ ! -f "$file" ]; do
        echo "$message"
        sleep 10
    done
}

# Función para esperar a que un archivo tenga un periodo superior a la cantidad especificada por period por defecto en 1 minuto
wait_for_file_period() {
    local file=$1
    local period=${2:-+1}
    local message="Esperando a que el archivo $file tenga un periodo superior a ${period} minuto"

    while [ $(find "$file" -mmin ${period} | wc -l) -eq 0 ]; do
        echo "$message"
        sleep 10
    done
}
